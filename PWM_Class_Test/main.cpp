

/**
 * Testprogramm der PWM Klasse:
 * Dieses Programm soll die Funkionalit�t der PWM Klasse pr�fen, sodass diese im Hauptprogramm benutzt werden kann.
 *
 */
#include "PWM.h"
#include "System.h"
#include "Config.h"
#include <cmath>

void main(void)
{
	System sys;
	PWM motorRight, motorLeft;
	GPIO dirLeft, dirRight, brakeLeft, brakeRight, enable;

	// Initialisierung des Systemobjektes
	sys.init(40000000);

	//TODO: Put your code here -> A3.2

    // Initialisierung der GPIO Pins für die Motorsteuerung
    dirLeft.init(&sys, CFG_MTR1_DIR_PORT, CFG_MTR1_DIR_PIN, CFG_MTR1_DIR_DIR, CFG_MTR1_DIR_PULLUP);
    dirRight.init(&sys, CFG_MTR2_DIR_PORT, CFG_MTR2_DIR_PIN, CFG_MTR2_DIR_DIR, CFG_MTR2_DIR_PULLUP);
    brakeLeft.init(&sys, CFG_MTR1_BRAKE_PORT, CFG_MTR1_BRAKE_PIN, CFG_MTR1_BRAKE_DIR, CFG_MTR1_BRAKE_PULLUP);
    brakeRight.init(&sys, CFG_MTR2_BRAKE_PORT, CFG_MTR2_BRAKE_PIN, CFG_MTR2_BRAKE_DIR, CFG_MTR2_BRAKE_PULLUP);
    enable.init(&sys, CFG_MTR_EN_PORT, CFG_MTR_EN_PIN, CFG_MTR_EN_DIR, CFG_MTR_EN_PULLUP);

    // Aktivierung des Motor Treibers
    enable.write(1);
    brakeLeft.write(!CFG_MTR_BRAKE_TRUE);
    brakeRight.write(!CFG_MTR_BRAKE_TRUE);

    // Initialisierung der PWM Kan�le f�r die beiden Motoren
    motorLeft.init(&sys, &dirLeft, CFG_MTR_PWM_PORT, CFG_MTR_PWM_PIN1, CFG_MTR_PWM_INV, CFG_MTR_PWM_FREQ);
    motorRight.init(&sys, &dirRight, CFG_MTR_PWM_PORT, CFG_MTR_PWM_PIN2, CFG_MTR_PWM_INV, CFG_MTR_PWM_FREQ);
    motorLeft.setDuty(0.5);
    motorRight.setDuty(0.5);

    // Endlosschleife, damit das Programm weiterl�uft
    while(1)
    {
        // Tastgrad schrittweise von -60% bis 60% und zur�ck �ndern
        for(int duty = -60; duty <= 60; duty++)
        {
            motorRight.setDuty(duty / 100.f);
            motorLeft.setDuty(duty / 100.f);
            sys.delayUS(30000);
        }
        for(int duty = 60; duty >= -60; duty--)
        {
            motorRight.setDuty(duty / 100.f);
            motorLeft.setDuty(duty / 100.f);
            sys.delayUS(30000);
        }


    }

}
