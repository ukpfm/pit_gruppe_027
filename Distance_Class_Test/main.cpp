/**
 * main.c
 */
#include "System.h"
#include "Distance.h"
#include "Timer.h"
#include "ErrorCodes.h"
#include "GPIO.h"


System sys;
Distance sensor0;
GPIO led;
Timer measure;

// changeable variables are declared here
uint32_t echoPort = CFG_ECHO1_PORT;
uint32_t echoPin = CFG_ECHO1_PIN;
uint32_t triggerPort = CFG_TRIG1_PORT;
uint32_t triggerPin = CFG_TRIG1_PIN;

uint32_t timerBase = CFG_US_TIMER_BASE;
uint32_t frequency = CFG_US_TIMER_FREQ;

uint32_t ledPort = GPIO_PORTA_BASE;
uint32_t ledPin = GPIO_PIN_4;


void ISR0() {

    sensor0.edgeDetect();

}

void ControlISR() {

    sensor0.trigger();
    measure.clearInterruptFlag();

}

void main() {

    sys.init(CFG_SYS_FREQ);

    // initializes the GPIO pin (led) to indicate obstacles
    led.init(&sys, ledPort, ledPin, GPIO_DIR_MODE_OUT, false);

    // initializes the distance sensor
    sensor0.init(&sys, echoPort, echoPin, triggerPort, triggerPin, &ISR0);

    // initializes the timer to trigger the measurement periodically
    measure.init(&sys, timerBase, &ControlISR, frequency);

    // starts the timer
    measure.start();

    while(1) {

        // makes the LED glow, when the sensor detects some obstacle within 30cm in front of it
        led.write(sensor0.getDistance() < 30);

    }

}
