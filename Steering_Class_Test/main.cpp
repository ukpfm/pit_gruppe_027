#include "PWM.h"
#include "Steering.h"
#include "System.h"
#include "Timer.h"
#include "Config.h"

Timer timer1;
Steering steering;
PWM motorRight, motorLeft;
System sys;

void ISR() {
    // clear the timer interrupt
    timer1.clearInterruptFlag();

    steering.calcValue();

    // get values
    float x = steering.getXValue();
    float y = steering.getYValue();
    float leftSpeed = steering.getLeftSpeed();
    float rightSpeed = steering.getRightSpeed();

    // send to plot
    sys.setDebugVal("xValue", x * 100);
    sys.setDebugVal("yValue", y * 100);
    sys.setDebugVal("leftSpeed", leftSpeed * 100);
    sys.setDebugVal("rightSpeed", rightSpeed * 100);

    // maybe use debugTimer like in Remote_Class_Test/main.cpp
    sys.sendDebugVals();

    // control motors
    motorLeft.setDuty(leftSpeed);
    motorRight.setDuty(rightSpeed);
}

int main(void) {
    // construct all objects
    System sys;
    GPIO dirLeft, dirRight;
    
    // start system and run at 40 MHz
    sys.init(40000000);
    sys.enableFPU();

    // initialize Steering
    steering.init(&sys, 
        CFG_JOY_XAX_MOD, CFG_JOY_YAX_MOD, 
        CFG_JOY_XAX_SSQ, CFG_JOY_YAX_SSQ, 
        CFG_JOY_XAX_ANI, CFG_JOY_YAX_ANI);

    // initialize GPIO pins for motor control
    dirLeft.init(&sys, CFG_MTR1_DIR_PORT, CFG_MTR1_DIR_PIN, CFG_MTR1_DIR_DIR, CFG_MTR1_DIR_PULLUP);
    dirRight.init(&sys, CFG_MTR2_DIR_PORT, CFG_MTR2_DIR_PIN, CFG_MTR2_DIR_DIR, CFG_MTR2_DIR_PULLUP);

    // initialize PWM channels for both motors
    motorLeft.init(&sys, &dirLeft, CFG_MTR_PWM_PORT, CFG_MTR_PWM_PIN1, CFG_MTR_PWM_INV, CFG_MTR_PWM_FREQ);
    motorRight.init(&sys, &dirRight, CFG_MTR_PWM_PORT, CFG_MTR_PWM_PIN2, CFG_MTR_PWM_INV, CFG_MTR_PWM_FREQ);
    motorLeft.setDuty(0.5);
    motorRight.setDuty(0.5);

    // initialize timer to run the isr every hundredth second
    timer1.init(&sys, TIMER1_BASE, &ISR, 10);
    // start the timer
    timer1.start();

    // keep the processor busy in between interrupts
    while (1) {}
}
