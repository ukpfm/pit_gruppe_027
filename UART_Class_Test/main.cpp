

/*
 * Author: Robin Rau
 *
 * Test program for the UART Communication with the remote Control. The characters are sent using UART (see System.cpp) every 1s and received every 1s.
 * For better user understanding, the characters sent represent the integer values 0 to 11.
 * To ensure that the bytes are in the right order, after each read the program checks whether the package is too short or too long. If either case
 * exists, the transmission wasn't correct and the status is set to false (-> On-Board LED is turned off).
 * If the transmission has the correct length, the status is true and the LED is turned on.
 * The stability of the transmission can therefore be seen via the on-board LED.
 * Set the UART Settings in the Config.h !
 */


#include "System.h"
#include "Timer.h"
#include "GPIO.h"
#include "UART.h"



Timer timer1;
UART communication;
GPIO led1;

bool status = true;
char receiveArray[12] = {};
char sendArray[12] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };

void ISR()
{
    //TODO: Put your code here -> A4.2
    // Clear the timer interrupt
    timer1.clearInterruptFlag();
    /* Send and receive characters that represent the integer values 0 to 11
     */
    communication.send(sendArray, 12);
    status = communication.receive(receiveArray, 12);

    // also check if the received messages are equal to the send messages
    for (size_t i = 0; i < 12; i++) {
      if (receiveArray[i] != sendArray[i]) {
        status = false;
        break;
      }
    }

    // reset the received array
    memset(receiveArray, 0, 12);
    // status = !status;
    /* Set the LED based on whether the message has the correct length
     */
    led1.write(status);
}

void main(void)
{
    System sys;

    sys.init(CFG_SYS_FREQ);
    sys.enableFPU();

    //TODO: Put your code here -> A4.2
    
    // initialize the gpio pin for the led
    led1.init(&sys, CFG_LED1_PORT, CFG_LED1_PIN, CFG_LED1_DIR);
    // initialze one uart module
    communication.init(&sys, CFG_UART_PORT, CFG_UART_TX, CFG_UART_RX);
    // initialize timer to run the isr every sedond
    timer1.init(&sys, TIMER1_BASE, &ISR, 1);
    // start the timer
    timer1.start();

    // Keep the processor busy inbetween interrupts
    while(1)
    {
        //TODO: Put your code here -> A4.2

    }
}
