// Uncomment following #define to use the precompiled PWM library instead of
// the code in this file.
//#define USE_PWM_LIBRARY

#ifndef USE_PWM_LIBRARY


#include <PWM.h>


PWM::PWM() // @suppress("Class members should be properly initialized")
{

}

PWM::~PWM() // @suppress("Class members should be properly initialized")
{

}


void PWM::init(System *sys, GPIO *dir, uint32_t portBase, uint32_t pin,
               bool invert, uint32_t freq) {
    /*
     * Initialize a PWM module with the right PWM generator to drive an
     * H-Bridge connected to pin1 and pin2. It will run at 5kHz by default.
     * Outputs won't be active until the user sets a duty cycle.
     *
     * sys:        Pointer to the current System instance. Needed to get CPU
     *             clock frequency.
     * dir:        Zeiger auf den Richtungspin des Motors.
     * portBase:   Base address of the port with the output pins
     *             (e.g. GPIO_PORTA_BASE).
     * pin1:       The two output pins this object will use (e.g. GPIO_PIN_6).
     *             The lower pin will be used for forwards, the higher for
     *             backwards.
     * freq:       optional parameter to set the PWM frequency. Default: 5kHz
     */

    // store pointer to the given System object, the pointer to the direction pins and the PWMInvert parameter
    this->sys = sys;
    this->dir = dir;

    // PWM::setDuty(float duty) uses floats
    sys->enableFPU();

    //TODO: This is the custom code for task A3.1

    /*
     * This variable stores the index of the correct pin mapping needed for the configuration in PWM_PIN_MAPPING
     * If the value remains -1 no pin mapping could be found
     */
    int finalPinMapping = -1;

    // this loop finds the correct pin mapping
    for (uint32_t i = 0; i < PWM_PINOUT_COUNT; ++i) {
        if (this->PWM_PIN_MAPPING[i][PWM_PINS] == (portBase | pin)) {
            finalPinMapping = i;
            break;
        }
    }

    // return error if no pin mapping could be found
    if (finalPinMapping == -1) {
        sys->error(PWMWrongPins, &portBase, &pin, &dir);
    }

    // enable the GPIO port and wait for ready
    uint32_t sysCtlPort = PWM_PIN_MAPPING[finalPinMapping][SYSCTL_PORT];
    SysCtlPeripheralEnable(sysCtlPort);
    while(!SysCtlPeripheralReady(sysCtlPort)){}

    // enable the pins
    uint32_t pinConfig = PWM_PIN_MAPPING[finalPinMapping][PIN_CONFIG];
    GPIOPinTypePWM(portBase, pin);
    GPIOPinConfigure(pinConfig);

    // Set variables of PWM object
    // Set PWM Base
    // First 8 entries use module 0, the other 8 use module 1
    this->base = (finalPinMapping < 8) ? PWM0_BASE : PWM1_BASE;
    // gen0 is 0x40 and other generators are multiples of this value (pwm.h)
    // With this formula we get the generators according to the table on page 1233
    // We divide with 2 because each 2 configurations in PWM_PIN_MAPPING use one generator
    // Then we use modulo 4, because after the 4th generator we need to start to count from 0 again (page 1233)
    this->gen = PWM_GEN_0 + ((finalPinMapping / 2) % 4) * 0x40;
    // Assign out pin
    // pwm.h shows, that the output pin has the generator as a base address, so it needs to be added to the generator address
    // Because we have 8 pins on each module and 2 separate modules, we need to start counting from 0 again after we passed the 8th pin,
    // That's what modulo 8 does (page 1233)
    this->out = this->gen + (finalPinMapping % 8);
    // Set pin bit
    // The bit's position depends on the pin number. The first pin's bit is on the LSB, the last pin's bit is on the MSB
    this->outBit = 1 << (finalPinMapping % 8);

    // enable PWM peripheral and wait for ready
    int pwmPeriph = (finalPinMapping <8) ? SYSCTL_PERIPH_PWM0 : SYSCTL_PERIPH_PWM1;
    SysCtlPeripheralEnable(pwmPeriph);
    while(!SysCtlPeripheralReady(pwmPeriph)){}

    // configure PWM generator
    PWMGenConfigure(this->base, this->gen, PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC);
    PWMGenPeriodSet(this->base, this->gen, this->clockFreq / freq);

    // set output state, currently deactivated
    PWMOutputState(this->base, this->outBit, 0);
    PWMOutputInvert(this->base, this->outBit, invert);
    PWMOutputUpdateMode(this->base, this->outBit, PWM_OUTPUT_MODE_NO_SYNC);

    // calculate and set frequency
    uint32_t clockDiv = sys->getPWMClockDiv();
    uint32_t sysClock = sys->getClockFreq();
    this->clockFreq = sysClock / clockDiv;
    this->setFreq(freq);

    // enable generator
    PWMGenEnable(this->base, this->gen);
}

void PWM::setFreq(uint32_t freq) {
    /*
     * Set the frequency at which the PWM output will be driven.
     *
     * freq: frequency in Hz. A too low frequency will deactivate the outputs.
     */

    //TODO: This is the custom code for task A3.1

    /*
     * You will have to add an optional parameter "adjustPWMClockDiv", with the default value "false",
     * because this feature can only be accessed under certain conditions.
     *
     * This method is allowed to change the divisor, only if this variable is explicitly set to "true".
     */
    bool adjustPWMClockDiv = false;

    uint32_t clockDiv = this->sys->getPWMClockDiv();
    uint32_t sysClock = this->sys->getClockFreq();
    this->clockFreq = sysClock / clockDiv;

    uint32_t period = this->clockFreq / freq;

    // check if period value is to big (timer is 16 bit)
    if (period > 0xFFFF) {
        // if parameter adjustPWMClockDiv is true, we can change the divider
        if (adjustPWMClockDiv) {

            // calculate new divider
            uint32_t newClockDiv = clockDiv;
            // max clock divider is 64 (pwm.h)
            while (period > 0xFFFF && newClockDiv <= 64) {
                newClockDiv *= 2;
                this->clockFreq = sysClock / newClockDiv;
                period = this->clockFreq / freq;
            }

            if (newClockDiv <= 64) this->sys->setPWMClockDiv(newClockDiv);
            // if no fitting clock divider could be found, we set the maximum duty cycle
            else setDuty(1);
        }
        // if period value to big, we set the maximum duty cycle
        else setDuty(1);
    }

    PWMGenPeriodSet(this->base, this->gen, period);
    PWMGenConfigure(this->base, this->gen, PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC);
    PWMGenEnable(this->base, this->gen);
}

void PWM::setDuty(float duty)
{
    /*
     * Set the duty cycle of the PWM output. The sign of the value determines
     * which one of the pins puts the pwm signal out. Negative values activate
     * the reverse pin, positive values the forward pin. A value of 0.0f (or
     * very close to 0.0f) deactivates the output.
     *
     * duty: duty cycle for the motor between -1.0f and 1.0f.
     */

    //TODO: This is the custom code for task A3.1

    // if duty cycle is out of range, return
    if (duty < -1.0f || duty > 1.0f) return;

    // set direction pin
    if (duty < 0) {
        dir->write(0);
        // make duty positive for pulse width calculation
        duty = -duty;
    }
    else dir->write(1);

    // calculate pulse width
    int period = PWMGenPeriodGet(this->base, this->gen);
    int pulseWidth = (period - 1) * duty;

    // deactivate output if value is close 0.0f
    if (pulseWidth == 0) {
        PWMOutputState(this->base, this->outBit, 0);
        return;
    }

    // set new pulse width
    PWMPulseWidthSet(this->base, this->out, pulseWidth);
    PWMOutputState(this->base, this->outBit, 1);
}

#endif
