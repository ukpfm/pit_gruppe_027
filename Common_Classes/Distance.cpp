// Uncomment following #define to use the precompiled ADC library instead of
// the code in this file.
//#define USE_DISTANCE_LIBRARY

#ifndef USE_DISTANCE_LIBRARY

#include "Distance.h"
#include "ErrorCodes.h"
#include "GPIO.h"
#include "Timer.h"
#include "ADC.h"

Distance::Distance() // @suppress("Class members should be properly initialized")
{

}

Distance::~Distance() // @suppress("Class members should be properly initialized")
{

}

void Distance::init(System *sys, uint32_t echoPort, uint32_t echoPin,
                    uint32_t triggerPort, uint32_t triggerPin, void (*ISR)(void)) {
    /*
     * Saves the configurations of the echo and trigger port/pin and the System Object.
     * Also creates a timer for measurement.
     */

    this->distanceSys = sys;

    // TODO: This is the custom code for task A5.1

    // initialize control pins for the supersonic sensor
    this->triggerObject.init(this->distanceSys, triggerPort, triggerPin, GPIO_DIR_MODE_OUT, false);
    this->echoObject.init(this->distanceSys, echoPort, echoPin, GPIO_DIR_MODE_IN, false);

    int index = getConfigIndex(echoPort, echoPin);
    if (index < 0) this->distanceSys->error(GPIOWrongConfig, &triggerPort);

    // enable peripheral for echo pin
    switch (this->config[index][0]) {
        case GPIO_PORTA_BASE: SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA); break;
        case GPIO_PORTB_BASE: SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB); break;
        case GPIO_PORTC_BASE: SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC); break;
        case GPIO_PORTD_BASE: SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD); break;
        case GPIO_PORTE_BASE: SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE); break;
        default: this->distanceSys->error(GPIOWrongConfig);
    }
    this->distanceSys->delayCycles(5);

    // define which timer base to take
    this->timerBase = this->config[index][2];

    // define which pin is the timer
    GPIOPinTypeTimer(echoPort, echoPin);
    // define pin as a wide timer
    GPIOPinConfigure(this->config[index][3]);

    // enable peripheral for timer
    switch (this->timerBase) {
        case WTIMER0_BASE: SysCtlPeripheralEnable(SYSCTL_PERIPH_WTIMER0); break;
        case WTIMER1_BASE: SysCtlPeripheralEnable(SYSCTL_PERIPH_WTIMER1); break;
        case WTIMER2_BASE: SysCtlPeripheralEnable(SYSCTL_PERIPH_WTIMER2); break;
        case WTIMER3_BASE: SysCtlPeripheralEnable(SYSCTL_PERIPH_WTIMER3); break;
        case WTIMER4_BASE: SysCtlPeripheralEnable(SYSCTL_PERIPH_WTIMER4); break;
        case WTIMER5_BASE: SysCtlPeripheralEnable(SYSCTL_PERIPH_WTIMER5); break;
        default: this->distanceSys->error(TimerWrongConfig, &timerBase);
    }
    this->distanceSys->delayCycles(5);

    // define timer A as a periodic timer
    TimerConfigure(this->timerBase, TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_PERIODIC);
    // define period in which the ISR is called
    uint32_t loadValue = TIMER_PERIOD_US * (this->distanceSys->getClockFreq() / 1000000) - 1;
    TimerLoadSet(this->timerBase, TIMER_A, loadValue);

    // define which ISR should be called
    TimerIntRegister(this->timerBase, TIMER_A, ISR);
    // enable ISR call
    TimerIntEnable(this->timerBase, TIMER_TIMA_TIMEOUT);
}

int Distance::getConfigIndex(uint32_t echoPort, uint32_t echoPin) {
    // find the configuration, which fits the given pin
    for (int i = 0; i < CONFIG_SIZE; i++) {
        if (echoPort == config[i][0] && echoPin == config[i][1]) return i;
    }
    return -1;
}

void Distance::trigger() {
    /*
     * Triggers the measurement. The measurement is started when a pulse with a length
     * of minimum 10 us occurs at the trigger pin.
     */

    // TODO: This is the custom code for task A5.1

    // give a signal to start the measurement routine
    this->triggerObject.write(true);
    this->distanceSys->delayUS(CFG_TRIG_TIME);
    this->triggerObject.write(false);

    // reset edge detection counter
    this->firstEdge = 0;
    this->secondEdge = 0;

    // start timer
    TimerEnable(this->timerBase, TIMER_A);
}

void Distance::edgeDetect() {
    /*
     * The class method which is called during interrupts.
     * When either edge is detected, the timer value is been stored.
     * If two values are stored, the calcDistance Method is called to
     * calculate the duration of the pulse and therefore the distance
     * to the nearest object.
     * Disables the timer and resets the measuring variable.
     */

    // TODO: This is the custom code for task A5.1

    // clear interrupt, in order to get no collision with other ISR calls
    TimerIntClear(this->timerBase, TIMER_TIMA_TIMEOUT);

    // compare this to the previous edge named "edge"
    bool currentEdge = this->echoObject.read();

    // break if edge high -> low was detected, or the time exceeds 10ms
    // (which is more than enough time to notice any obstacle in range)
    if (this->edge && !currentEdge || (this->firstEdge + this->secondEdge) * TIMER_PERIOD_US > 10000) {

        // measurement finished, reset timer
        this->edge = false;
        // stop timer
        TimerDisable(this->timerBase, TIMER_A);
        // calculate distance
        calcDistance();

    }

    // count until swap high to low (time from measurement start till signal detected)
    else if (this->edge) this->secondEdge++;

    // count until swap low to high (time till measurement start)
    else if (!currentEdge) this->firstEdge++;

    // notice edge (start of measurement)
    else this->edge = true;
}

void Distance::calcDistance() {
    // TODO: This is the custom code for task A5.1

    // calculates the time of the HIGH pulse
    uint64_t timeUS = this->secondEdge * TIMER_PERIOD_US;
    // *100 for m -> cm ; /1 000 000 for s -> us
    float soundSpeedCMpUS = SOUNDVELOCITY / 10000;
    // distance in cm
    this->distance = timeUS * soundSpeedCMpUS / 2;

    // default value, if no obstacle has been found
    if (this->distance > MAX_DISTANCE || this->distance == 0) this->distance = MAX_DISTANCE;
}

float Distance::getDistance() {
   return this->distance;
}


#endif

