#include "Steering.h"
#include "inc/hw_memmap.h"
#include "math.h"

// clamps a to be between b and c inclusively
#define CLAMP(a,b,c) fminf(fmaxf((a),(b)),(c))

Steering::Steering() // @suppress("Class members should be properly initialized")
{

}

Steering::~Steering() // @suppress("Class members should be properly initialized")
{

}

void Steering::init(System *sys, uint32_t baseX, uint32_t baseY, uint32_t sampleSeqX, uint32_t sampleSeqY, uint32_t analogInX, uint32_t analogInY) {
    // Initialize the ADC objects to read the joystick values
    xValue.init(sys, baseX, sampleSeqX, analogInX);
    yValue.init(sys, baseY, sampleSeqY, analogInY);
}

void Steering::calcValue() {
    /*
     * The joystick values given by the UART Communication are between -100 and 100.
     * Since the joystick is returns small values even when its centered, these values are
     * grounded to zero.
     * Furthermore, the x axis is inverted in comparison to the intuition.
     */

    //TODO: This is the custom code for task A6.1

    // "the x axis is inverted in comparison to the intuition" => invert again, to make it intuitive
    float x = -1.f * getXValue();
    float y = getYValue();

    /*
     * velocity: tilt of joystick
     * angle:    direction of joystick (y+ being 0 degrees and x+ being 90 degrees)
     *
     * vr = velocity * (cos(alpha) + sin(alpha))
     * vl = velocity * (cos(alpha) - sin(alpha))
     * with
     * y = cos; x = sin
     *
     * [+/-]90 degrees rotates the robot on the spot
     * vr / vl have to be clamped or scaled to be in range [-1..1]
     * we choose clamp to also have max velocities when driving straight
     *
     * assertion with x = cos und y = sin falls apart at x = y = 1.0
     * then the velocity sqrt(2) is greater than 1.0 so we set a max
     */
    
    float velocity = fmin(sqrt(x * x + y * y), 1.f);
    leftSpeed = velocity * CLAMP(y - x, -1.f, 1.f);
    rightSpeed = velocity * CLAMP(y + x, -1.f, 1.f);
}

float Steering::getLeftSpeed() {
    return leftSpeed;
}

float Steering::getRightSpeed() {
    return rightSpeed;
}

float Steering::getXValue() {
    /*
     * readVolt = [0..3.3V] = read * 3.3f / 4095.0f
     * so could also be implemented like
     * return xValue.read / 4095.0f;
     * might be less rounding errors
     */
    return (xValue.readVolt() / 3.3f) * 2.f - 1.f;
}

float Steering::getYValue() {
    // for explanation see Steering::getXValue()
    return (yValue.readVolt() / 3.3f) * 2.f - 1.f;
}

