// Uncomment following #define to use the precompiled ADC library instead of
// the code in this file.
// #define USE_UART_LIBRARY

#include "Config.h"
#ifndef USE_UART_LIBRARY

#include "UART.h"

UART::UART() // @suppress("Class members should be properly initialized")
{

}

UART::~UART() // @suppress("Class members should be properly initialized")
{

}

void UART::init(System *sys, uint32_t portBase, uint32_t tx, uint32_t rx) {
    /*
     * System *sys: pointer to current system instance
     * uint32_t portBase: GPIO port address of desired UART module
     * uint32_t tx: GPIO pin used by the UART module to send
     * uint32_t rx: GPIO pin used by the UART module to receive
     */

    /*
     * The goal of this method is to enable and configure the UART and GPIO module used for the communication.
     * First, figure out which UART module should be configured based on the GPIO Port/Pin Combination given in the
     * argument. After that, enable the UART module and GPIO module, configure the GPIO pins as UART pins, and set the
     * UART configuration.
     * Desired Configuration:
     * Word Length = 8
     * Stop Bit = 1
     * Parity Bit = None
     * Lastly, enable the FIFOs.
     */

    this->sys = sys;

    //TODO: This is the custom code for task A4.1
    
    /*
     * It is smart to use an array instead of a reference,
     * because we need the index for calculation and configuration later.
     *
     * Initialize with UART_MODULES_COUNT in order to identify the absence of a valid configuration.
     */
    size_t uart_index = UART_MODULES_COUNT;

    // search in array if given arguments are a valid config
    for (size_t i = 0; i < UART_MODULES_COUNT; i++) {
        if (this->uartConfig[i].portBase_pin == (portBase | tx | rx)) {
            uart_index = i;
            break;
        }
    }

    // if the arguments are not found as a valid config print error
    if (uart_index == UART_MODULES_COUNT ) {
        sys->error(GPIOWrongConfig, &uart_index);
    }

    /*
     * Enable UART Module by offsetting SYSCTL_PERIPH_UART0 by uart_index.
     * This works because the UART Module SYSCTL_PERIPH_UARTx = SYSCTL_PERIPH_UART0 + x.
     * See sysctl.h
     */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0 + uart_index);

    /*
     * Enable GPIO Module.
     * Taken from Common_Classes/GPIO.cpp
     */
    switch (portBase) {
        case GPIO_PORTA_BASE: SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA); break;
        case GPIO_PORTB_BASE: SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB); break;
        case GPIO_PORTC_BASE: SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC); break;
        case GPIO_PORTD_BASE: SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD); break;
        case GPIO_PORTE_BASE: SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE); break;
        case GPIO_PORTF_BASE: SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF); break;
        default: sys->error(GPIOWrongConfig, &portBase);
    }

    /*
     * Wait for the UART0 module to be ready.
     * "TivaWare(TM) Treiberbibliothek" page 579
     */
    while (!SysCtlPeripheralReady(SYSCTL_PERIPH_UART0 + uart_index)) {}

    /*
     * This function configures the pin mux that selects the peripheral function
     * associated with a particular GPIO pin.
     */
    GPIOPinConfigure(this->uartConfig[uart_index].rx);
    GPIOPinConfigure(this->uartConfig[uart_index].tx);

    // Set UART at the peripheral function for the pins.
    GPIOPinTypeUART(portBase, tx | rx);

    /* 
     * Base Address of the UART Module 
     * Taken from exercise sheet p. 19
     * UART(x+1) BASE = UARTx BASE + 0x1000
     */
    this->base = UART0_BASE + (0x1000 * uart_index);

    /*
     * Initialize the UART. Set the baud rate, number of data bits, turn off
     * parity, number of stop bits.
     * */
    UARTConfigSetExpClk
        (
            this->base,
            sys->getClockFreq(),
            CFG_BAUD_RATE,
            (
                UART_CONFIG_WLEN_8
                | UART_CONFIG_STOP_ONE
                | UART_CONFIG_PAR_NONE
            )
        );
}

void UART::send(const char *transmit, uint8_t length)
{
    /*
     * Sends the message that is given in the argument.
     *
     * *transmit: Pointer to the char array which stores the message
     * length: Length of the char array
     */

    //TODO: This is the custom code for task A4.1

    // send the sync word as the first message
    UARTCharPut(this->base, SYNCWORD);

    // send each character of the transmit array
    for (size_t i = 0; i < length; ++i) {
        UARTCharPut(this->base, transmit[i]);
    }
}

bool UART::checkAvailability(){
    if(UARTCharsAvail(this->base))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool UART::receive(char *receive, uint8_t length)
{
    /*
     * The goal of the receive method is reading the receive FIFO, decoding the incoming message by removing start/stop
     * and parity bit(s) and searching the decoded message for the synchronization word (view Config.h).
     * First, all incoming messages are being trashed until the synchronization word is found.
     * After that, the message is written in the receive array provided in the argument.
     * If the message is too short the method stops and returns false.
     * If the message is too long the method clears the rest of the FIFO until it's empty and returns false.
     * If the message is as long as expected the method returns true.
     */

    //TODO: This is the custom code for task A4.1

    int32_t message;

    // First, all incoming messages are being trashed until the synchronization word is found.

    do {
        if (!checkAvailability()) return false;
        message = UARTCharGet(this->base);

    // check if message could be decoded is already done by checking for SYNCWORD
    } while (message != SYNCWORD);

    for (size_t i = 0; i < length; i++) {
        // if the message is too short the method stops and returns false
        if (!checkAvailability()) return false;
        message = UARTCharGet(this->base);

        receive[i] = message;
    }

    // if the message is too long the method clears the rest of the FIFO until it's empty and returns false
    if (checkAvailability()) {
        while (checkAvailability()) {
            UARTCharGet(this->base);
        }
        return false;
    }

    return true;
}
#endif
